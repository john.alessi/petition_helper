var links = []

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {

    if(request.cmd == 'read_file') {
        $.ajax({
            url: chrome.extension.getURL('change_org_insert/test.html'),
            dataType: 'html',
            success: sendResponse
        })
        return true
    }

    if(request.cmd == 'set_links') {
        links = request.links
    }

    if(request.cmd == 'get_links') {
        sendResponse({
            links: links
        })
    }

    if(request.cmd == 'remove_link') {
        links = links.filter(e => e !== request.url)
        chrome.storage.local.get({'already-signed': []}, function(result) {
            already_signed = result['already-signed']
            chrome.storage.local.set({'already-signed': already_signed.concat(request.url)})
        })
    }
})