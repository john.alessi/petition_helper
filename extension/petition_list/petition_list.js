var petition_url_list = $('#50e6e366-6910-4c81-aeab-d4b734ecd370')
if (petition_url_list.length > 0) {
    var links = []
    chrome.storage.local.get({'already-signed': []}, function(result) {
        petition_url_list.find('a').each(function() {
            url = $(this).attr('href')
            if(result['already-signed'].indexOf(url) < 0) {
                links.push(url)
            }
        })  
        chrome.extension.sendMessage({
            cmd: 'set_links',
            links: links
        })
        if (links.length == 0) {
            $('body').html('<p>Looks like you\'ve signed or skipped all of the petitions on this list.</p>')
        }
        else {
            window.location = links[0]
        }
    })
}