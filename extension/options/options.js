document.getElementById("clear-data").addEventListener('click', () => {
    chrome.storage.local.clear(() => chrome.extension.getBackgroundPage().alert('Extension data cleared.'))
})