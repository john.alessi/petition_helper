var links = []

chrome.extension.sendMessage({cmd: 'get_links'}, function(response) {
	
	links = response.links

	if(links.length > 0) {
		chrome.extension.sendMessage({cmd: 'read_file'}, function(html) {

			var cf_error = $('.cf-error-code')

			$('body').children().hide()
			$('body').append('<div id="parent-view" height="100%"></div>')
			$('#parent-view').html(html)
			links.forEach(e => $('#sidebar').append('<a class="link-button" href=' + e + '>' + e + '</a><hr/>'))


			if (cf_error.length > 0) {
				$('#skip-button').hide()
				$('#change-org-view').append('<p>Something broke on change.org\'s end, give it a minute and try again.</p>')
			}
			else {
				var title = $('.petition-title')
				var coverPhoto = $('.hidden-xs').find('img').first()
				var form = $('.sign-form')
				var login_button = $('a:contains("Log in")')
				var description = $('.js-description-content')
				var victory = $('.symbol-victory.symbol-xl').parent()
				var already_signed = form.length == 0 && victory.length == 0

				coverPhoto.removeClass()
				title.removeClass()
				title.addClass('title')

				login_button.appendTo('#change-org-view')
				title.appendTo('#change-org-header')
				coverPhoto.appendTo('#cover-photo')
				form.appendTo('#change-org-view')
				victory.appendTo('#change-org-view')
				description.appendTo('#description')

				if (already_signed) {
					$('#change-org-view').append('<p>Looks like you already signed this one.</p>')
				}

				$('#skip-button').click(() => markPetitionAsDone(window.location.href, links))
				form.find('.btn').click(() => markPetitionAsDone(window.location.href, links))
			}
		})
	}
})

function markPetitionAsDone(url, links) {
	chrome.extension.sendMessage({
		cmd: 'remove_link',
		url: url
	},
	function() {
		if(links.length <= 1) {
			$('body').html('<p>Looks like you\'ve signed or skipped all of the petitions on this list.</p>')
		}
		else {
			window.location = links.find(e => e !== url)
		}
	})
}