import sys
import os
import pickle
from pathlib import Path
from selenium.webdriver import Chrome, ChromeOptions
from selenium.common.exceptions import InvalidArgumentException, NoSuchElementException

COOKIES_PATH = '.changeorg/cookies.pkl'
CHANGE_ORG_HOME = 'https://www.change.org/'
CHROME_EXTENSION_URL = "https://chrome.google.com/webstore/detail/changeorg-petition-tracke/ffphmliiaihbkopplcpjcahpeljadame/related?hl=en&authuser=0"

HTML_HEAD = '''
<html>
    <body>
        <p>We are now on the chrome store!  Come back to this page once you've installed the extension to get started.</p>
        <a href="%s">Get the chrome extension here</a>
        <hr />
        <h3>List of Petitions</h3>
        <ul id="50e6e366-6910-4c81-aeab-d4b734ecd370">
'''

HTML_TAIL = '''
        </ul>
    </body>
</html>
'''

options = ChromeOptions()
options.add_argument('--headless')
driver = Chrome(options=options)

driver.get(CHANGE_ORG_HOME)
for cookie in pickle.load(open(str(Path.home()) + '/' + COOKIES_PATH, 'rb')):
    driver.add_cookie(cookie)

print(HTML_HEAD % CHROME_EXTENSION_URL)

for url in sys.stdin:
    try:
        driver.get(url)
        title = driver.find_element_by_xpath('//h1[@class="mtl mbxxxl xs-mts xs-mbxs type-break-word petition-title"]').get_attribute('innerHTML')
        print('<li><a href="%s">%s</a></li>' % (driver.current_url, title))
    except InvalidArgumentException:
        pass
    except NoSuchElementException:
        print('<li><a href="%s">%s</a></li>' % (driver.current_url, driver.current_url))

print(HTML_TAIL)
