ALLY_WIKI_LINK='https://docs.google.com/document/d/e/2PACX-1vSrT26HMWX-_hlLfiyy9s95erjkOZVJdroXYkU-miaHRk58duAnJIUWKxImRkTITsYhwaFkghS8sfIF/pub'
MY_LINKS='https://docs.google.com/document/d/e/2PACX-1vShSWkdzDKt2bkDb4XSfuzJ0Gq0RlG0u5IFFiyg2FpQyoh8MyiLTY56fnn8v6j4zKoFvUcsFrMidRNd/pub'
CARRD_CO='https://blacklivesmatters.carrd.co/'

for link in $CARRD_CO $ALLY_WIKI_LINK $MY_LINKS
do
	curl $link -m 5
done \
	| perl -ne 'while(/href.*?(https:\/\/www\.change\.org[^?&\"]*)/g){print "$1\n";}' \
	| sed -E 's/%25/%/g' \
	| uniq \
	| python build_petition_list.py \
	> petition_list.html
